.PHONY : all check-format clippy doc

# Run build, tests and examples, in debug mode
all:
	cargo c && cargo t

# Check code format, does not apply changes
check-format:
	cargo fmt --all -- --check

# Clippy linting for code, tests and examples with pedantic lints
clippy:
	cargo clippy --all-targets -- -W clippy::pedantic

# Create documentation without dependencies.
doc:
	cargo doc --no-deps
