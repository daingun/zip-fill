# zip-fill - Iterator Library

[Repository](https://gitlab.com/daingun/zip-fill)

[Crate registry](https://crates.io/crates/zip-fill)

[Documentation](https://docs.rs/zip-fill)

Zip two iterators, if they have different length, the shortest is extended
using a default value.

The two iterators must have the same `Item` type.

It is similar to `zip_longest` algorithm from the `itertool` python library.

## Example

```
use zip_fill::ZipFill;
let a = [ 1, 2, 3, 4];
let b = [ 1, 2];
let z: Vec<_> = a.iter().zip_longest(&b, &0).collect();
assert_eq!(vec!((&1, &1), (&2, &2), (&3, &0), (&4, &0)), z);
let z: Vec<_> = zip_fill::zip_longest_with(&a, &b, &0, |a, b| a + b).collect();
assert_eq!(vec!(2, 4, 3, 4), z);
```

## Requirements

Minimum tested rustc version: 1.44
